package site.one28;

import java.util.HashMap;

import site.one28.WordFrequency.Stream.*;

public class Main {


    public static void main(String[] args) {
        HashMap index = indexWords();
        String mostCommonWord = Counter.max(index);

        System.out.println(mostCommonWord);
    }

    private static HashMap indexWords() {

        //	 We're born alone, we live alone, we die alone.
        //   Only through our love and friendship can
        //	 we create the illusion for the moment
        //	 that we're not alone.

        HashMap w = new HashMap<String, Integer>();
        w.put("we're", 2);
        w.put("born", 1);
        w.put("alone", 4);
        w.put("we", 3);
        w.put("live", 1);
        w.put("die", 1);
        w.put("Only", 1);
        w.put("though", 1);
        w.put("our", 1);
        w.put("love", 1);
        w.put("and", 1);
        w.put("friendship", 1);
        w.put("can", 1);
        w.put("create", 1);
        w.put("the", 2);
        w.put("illusion", 1);
        w.put("for", 1);
        w.put("moment", 1);
        w.put("that", 1);
        w.put("not", 2);

        return w;
    }
}
