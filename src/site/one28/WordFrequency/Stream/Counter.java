package site.one28.WordFrequency.Stream;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class Counter {
    public static String max(Map<String, Integer> list) {
        return list
                .entrySet()
                .stream()
                .parallel()
                .max((entry1, entry2) ->
                        entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
    }
}
